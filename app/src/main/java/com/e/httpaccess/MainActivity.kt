package com.e.httpaccess

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.telephony.TelephonyManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.e.httpaccess.database.CallLogContentAccess
import com.e.httpaccess.recyclerviews.adapters.CallLogAdapter
import com.e.httpaccess.services.BackgroundService
import com.e.httpaccess.services.CallStatusReceiver
import com.e.httpaccess.viewmodels.MainActivityViewModel
import com.e.httpaccess.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSION_REQUEST_READ_CALL_LOG = 1000
        private const val PERMISSION_READ_PHONE_STATE = 1001
        private const val PERMISSION_READ_CONTACTS = 1002
    }

    private lateinit var mainActivityViewModel: MainActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPermission(Manifest.permission.READ_CALL_LOG, PERMISSION_REQUEST_READ_CALL_LOG) {
            initialiseViewModel()
        }
        checkPermission(Manifest.permission.READ_PHONE_STATE, PERMISSION_READ_PHONE_STATE) {
            registerCallStatusReceiver()
        }
        checkPermission(Manifest.permission.READ_CONTACTS, PERMISSION_READ_CONTACTS) {
        }

        rv_call_log.setHasFixedSize(true)

        mainActivityViewModel.callLogs.observe(this) {
            rv_call_log.adapter = CallLogAdapter(it)
        }

        mainActivityViewModel.serverFullAddress.observe(this, {
            tv_info.text = it
        })

        btn_start.setOnClickListener {
            Intent(this, BackgroundService::class.java).also {
                it.putExtra(BackgroundService.PORT_EXTRA, mainActivityViewModel.serverPort.value)
                it.putExtra(BackgroundService.IP4_EXTRA, mainActivityViewModel.serverIp4.value)
                startService(it)
            }
        }

        btn_stop.setOnClickListener {
            Intent(this, BackgroundService::class.java).also {
                stopService(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            if (requestCode == PERMISSION_REQUEST_READ_CALL_LOG) {
                initialiseViewModel()
            }
            if (requestCode == PERMISSION_READ_PHONE_STATE) {
                registerCallStatusReceiver()
            }
        }
    }

    private fun registerCallStatusReceiver() {
        val filter = IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED)
        registerReceiver(CallStatusReceiver(), filter)
    }

    private fun checkPermission(
        permission: String, permissionRequest: Int, permissionGrantedCallback: () -> (Unit)
    ) {
        val permissionCheck = ContextCompat.checkSelfPermission(
            this,
            permission
        )
        when (permissionCheck) {
            PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
                permissionGrantedCallback()
            }
            else -> {
                requestPermissions(
                    arrayOf(permission),
                    permissionRequest
                )
            }
        }
    }

    private fun initialiseViewModel() {
        mainActivityViewModel =
            ViewModelProvider(this, ViewModelFactory(CallLogContentAccess(this)))
                .get(MainActivityViewModel::class.java)
    }
}