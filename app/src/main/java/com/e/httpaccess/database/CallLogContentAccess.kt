package com.e.httpaccess.database

import android.content.Context
import android.database.Cursor
import android.provider.CallLog
import com.e.httpaccess.model.CallLogEntry
import java.util.*

class CallLogContentAccess(private val context: Context) : CallLogRepository {

    override fun getAllCallLogs(): List<CallLogEntry> {
        val callLogs = arrayListOf<CallLogEntry>()
        context.contentResolver.query(CallLog.Calls.CONTENT_URI, null, null, null, null)
            .use { cur ->
                if (cur!!.moveToFirst()) {
                    do {
                        val callLogEntry = callLogEntryFromCursor(cur)
                        callLogs.add(callLogEntry)
                    } while (cur.moveToNext())
                }
            }

        return callLogs
    }

    override fun getLogsFromDate(date: Date): List<CallLogEntry> {
        val callLogs = arrayListOf<CallLogEntry>()
        context.contentResolver.query(
            CallLog.Calls.CONTENT_URI,
            null,
            "${CallLog.Calls.DATE} >= ${date.time} ",
            null,
            null
        ).use { cur ->
            if (cur!!.moveToFirst()) {
                do {
                    callLogs.add(callLogEntryFromCursor(cur))
                } while (cur.moveToNext())
            }
        }
        return callLogs
    }

    private fun callLogEntryFromCursor(cur: Cursor): CallLogEntry {
        val stringName =
            cur.getString(cur.getColumnIndex(CallLog.Calls.CACHED_NAME))
        val stringDuration =
            cur.getString(cur.getColumnIndex(CallLog.Calls.DURATION))
        val stringNumber =
            cur.getString(cur.getColumnIndex(CallLog.Calls.NUMBER))
        val stringDate =
            cur.getString(cur.getColumnIndex(CallLog.Calls.DATE))

        return CallLogEntry(
            stringName ?: "",
            stringDuration ?: "",
            stringNumber ?: "",
            stringDate ?: ""
        )
    }
}