package com.e.httpaccess.database

import com.e.httpaccess.model.CallLogEntry
import java.util.*

interface CallLogRepository {
    fun getAllCallLogs(): List<CallLogEntry>
    fun getLogsFromDate(date: Date): List<CallLogEntry>
}