package com.e.httpaccess.database

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import com.e.httpaccess.model.ContactEntry

class PhoneContactContentAccess(private val context: Context) : PhoneContactRepository {
    override fun getContactByPhoneNumber(phoneNumber: String): List<ContactEntry> {
        val contact = mutableListOf<ContactEntry>()
        val uri = Uri.withAppendedPath(
            ContactsContract.Contacts.CONTENT_FILTER_URI,
            Uri.encode(phoneNumber)
        )
        context.contentResolver.query(
            uri,
            null,//arrayOf(ContactsContract.PhoneLookup.DISPLAY_NAME),
            null,
            null,
            null
        ).use { cur ->
            if (cur != null && cur.moveToFirst()) {
                do {
                    contact.add(contactEntryFromCursor(cur))
                } while (cur.moveToNext())
            }
        }
        return contact
    }

    private fun contactEntryFromCursor(cur: Cursor): ContactEntry {
        val name = cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME))
        return ContactEntry(name ?: "")
    }
}