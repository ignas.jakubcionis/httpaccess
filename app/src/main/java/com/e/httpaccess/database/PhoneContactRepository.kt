package com.e.httpaccess.database

import com.e.httpaccess.model.ContactEntry

interface PhoneContactRepository {
    fun getContactByPhoneNumber(phoneNumber: String): List<ContactEntry>
}