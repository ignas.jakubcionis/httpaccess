package com.e.httpaccess.model

data class CallLogEntry(
    val name: String,
    val duration: String,
    val number: String,
    val date: String
)