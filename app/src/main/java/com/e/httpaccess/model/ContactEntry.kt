package com.e.httpaccess.model

data class ContactEntry(val name: String)