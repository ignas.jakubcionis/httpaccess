package com.e.httpaccess.network

import java.io.IOException
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.ServerSocket
import java.util.*

object NetworkInfo {


    /**
     * @return available port number; -1 if no port in range available
     */
    fun findForOpenPort(
        address: InetAddress,
        startingPort: Int = 1,
        endingPort: Int = 65535
    ): Int {
        for (port: Int in startingPort..endingPort) {
            if (isPortOpen(address, port)) {
                return port
            }
        }
        return -1
    }

    private fun isPortOpen(address: InetAddress, port: Int): Boolean {
        return try {
            ServerSocket(port, 0, address).close()
            true
        } catch (e: IOException) {
            false
        }
    }

    fun findIp4(
        interfaces: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
    ): List<Inet4Address> {
        return interfaces.toList().filter {
            isWanted(it)
        }.mapNotNull {
            findIp4(it.inetAddresses)
        }
    }

    private fun isWanted(networkInterface: NetworkInterface): Boolean {
        return !networkInterface.isLoopback && !networkInterface.isVirtual && networkInterface.isUp
    }

    private fun findIp4(enumerator: Enumeration<InetAddress>): Inet4Address? {
        for (address in enumerator) {
            if (address is Inet4Address) {
                return address
            }
        }
        return null
    }
}