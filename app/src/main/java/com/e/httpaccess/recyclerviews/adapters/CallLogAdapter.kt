package com.e.httpaccess.recyclerviews.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.e.httpaccess.R

class CallLogAdapter(private val dataset: List<Pair<String, String>>) :
    RecyclerView.Adapter<CallLogAdapter.ItemViewHolder>() {

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.findViewById(R.id.tv_list_item_name)
        val tvCallTime: TextView = view.findViewById(R.id.tv_list_item_call_time)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)

        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.tvName.text = item.first
        holder.tvCallTime.text = item.second
    }

    override fun getItemCount(): Int = dataset.size
}