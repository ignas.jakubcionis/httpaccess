package com.e.httpaccess.restapis

object ApiStatistics {
    private val requestMap = mutableMapOf<Int, Int>()

    fun getCallCountAndIncrement(entityHash: Int): Int {
        var count: Int = requestMap[entityHash] ?: 0
        requestMap[entityHash] = ++count
        return --count
    }
}