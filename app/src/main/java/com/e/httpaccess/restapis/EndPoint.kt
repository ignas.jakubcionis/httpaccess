package com.e.httpaccess.restapis

data class EndPoint(val address: String, val name: String, val content: (String) -> String)