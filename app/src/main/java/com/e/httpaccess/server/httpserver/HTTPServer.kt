package com.e.httpaccess.server.httpserver

import fi.iki.elonen.NanoHTTPD

class HTTPServer(
    port: Int,
    private val onRequest: (session: IHTTPSession?) -> (Response)
) : NanoHTTPD(port) {

    override fun serve(session: IHTTPSession?): Response {
        return onRequest(session)
    }
}