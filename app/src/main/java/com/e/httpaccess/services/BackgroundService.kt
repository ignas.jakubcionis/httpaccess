package com.e.httpaccess.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.telephony.TelephonyManager
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.util.ObjectsCompat
import androidx.lifecycle.LifecycleService
import com.e.httpaccess.MainActivity
import com.e.httpaccess.R
import com.e.httpaccess.core.App
import com.e.httpaccess.database.CallLogContentAccess
import com.e.httpaccess.database.CallLogRepository
import com.e.httpaccess.database.PhoneContactContentAccess
import com.e.httpaccess.database.PhoneContactRepository
import com.e.httpaccess.model.CallLogEntry
import com.e.httpaccess.restapis.ApiStatistics
import com.e.httpaccess.restapis.EndPoint
import com.e.httpaccess.server.httpserver.HTTPServer
import fi.iki.elonen.NanoHTTPD
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class BackgroundService : LifecycleService() {

    private var httpServer: HTTPServer? = null
    private var callLogRepository: CallLogRepository? = null
    private var phoneContactRepository: PhoneContactRepository? = null

    private val notificationId = 1
    private val notificationChannelId = "Channel_1"
    private val contentTypeJSON = "application/json"
    private val contentType = "content-type"

    private var port: Int = -1
    private var ip4: String? = null
    private var serverStartTime: Date? = null
    private var callState: String = ""
    private var callNumber: String = ""

    private val apiMap: List<EndPoint> by lazy {
        listOf(
            EndPoint("/", getString(R.string.root)) { servicesResponse(it) },
            EndPoint("/status", getString(R.string.status)) { statusResponse(it) },
            EndPoint("/log", getString(R.string.log)) { logResponse(it) }
        )
    }

    private fun servicesResponse(contentType: String): String {
        if (contentType != contentTypeJSON) {
            return getString(R.string.error)
        }
        val json = JSONObject()
        json.put(getString(R.string.json_start), serverStartTime.toString())

        val array = JSONArray()

        apiMap.drop(1).forEach {
            val services = JSONObject().apply {
                put(it.name, "HTTP://$ip4:$port${it.address}")
            }
            array.put(services)
        }
        json.put(getString(R.string.json_services), array)

        return json.toString()
    }

    private fun statusResponse(contentType: String): String {
        if (contentType != contentTypeJSON) {
            return getString(R.string.error)
        }

        val isOngoing =
            callState == TelephonyManager.EXTRA_STATE_OFFHOOK || callState == TelephonyManager.EXTRA_STATE_RINGING
        val contactNames = phoneContactRepository?.getContactByPhoneNumber(callNumber)
            ?.joinToString(separator = ",") {
                it.name
            }

        return JSONObject().apply {
            put(
                getString(R.string.json_ongoing),
                isOngoing
            )
            put(
                getString(R.string.json_number),
                callNumber
            )
            put(
                getString(R.string.json_name),
                contactNames
            )
        }.toString()
    }

    private fun logResponse(contentType: String): String {
        if (contentType != contentTypeJSON) {
            return getString(R.string.error)
        }

        val app: App = application as App

        val logs: List<CallLogEntry> =
            callLogRepository?.getLogsFromDate(Date(app.startDate.time))!!

        val array = JSONArray()

        logs.map {
            array.put(JSONObject().apply {
                put(getString(R.string.json_beginning), Date(it.date.toLong()).toString())
                put(getString(R.string.json_duration), it.duration)
                put(getString(R.string.json_number), it.number)
                put(getString(R.string.json_name), it.name)
                put(
                    getString(R.string.json_times_queried),
                    ApiStatistics.getCallCountAndIncrement(
                        ObjectsCompat.hashCode(
                            it
                        )
                    )
                )
            })
        }

        return array.toString()
    }

    override fun onCreate() {
        super.onCreate()

        callLogRepository = CallLogContentAccess(applicationContext)
        phoneContactRepository = PhoneContactContentAccess(applicationContext)

        CallStatusReceiver.callState.observe(this) {
            callState = it
        }
        CallStatusReceiver.callNumber.observe(this) {
            callNumber = it
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName = getString(R.string.channel_name)
            val channelDescriptionText = getString(R.string.channel_description)
            createNotificationChannel(channelName, channelDescriptionText)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        intent?.let {
            port = it.getIntExtra(PORT_EXTRA, -1)
            ip4 = it.getStringExtra(IP4_EXTRA)
        }

        if (httpServer?.isAlive != true && port > -1) {

            httpServer = HTTPServer(port) {
                val contentType = it?.headers?.get(contentType)

                val endpoint = apiMap.firstOrNull { ep ->
                    ep.address == it?.uri
                }

                if (contentType == null || endpoint == null) {
                    return@HTTPServer NanoHTTPD.newFixedLengthResponse(
                        NanoHTTPD.Response.Status.BAD_REQUEST, NanoHTTPD.MIME_PLAINTEXT,
                        getString(R.string.error)
                    )
                }

                return@HTTPServer NanoHTTPD.newFixedLengthResponse(
                    NanoHTTPD.Response.Status.OK,
                    contentTypeJSON,
                    endpoint.content.invoke(
                        contentType
                    )
                )
            }

            httpServer!!.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false)
            serverStartTime = Calendar.getInstance().time
        }

        val notificationIntent = Intent(this, MainActivity::class.java)
        val notificationTitle = getString(R.string.notification_title)
        val notificationText = getString(R.string.notification_text)
        startForegroundWithNotification(
            notificationId,
            notificationChannelId,
            notificationIntent,
            notificationTitle,
            notificationText
        )

        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(
        name: String,
        descriptionText: String,
        channelId: String = notificationChannelId,
        importance: Int = NotificationManager.IMPORTANCE_DEFAULT
    ) {
        // Create the NotificationChannel
        val channel = NotificationChannel(channelId, name, importance)
        channel.description = descriptionText
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }


    private fun startForegroundWithNotification(
        id: Int,
        channelId: String,
        notificationIntent: Intent,
        title: String,
        text: String
    ) {
        val pendingIntent = PendingIntent.getActivity(
            this, 0,
            notificationIntent, 0
        )

        startForeground(
            id, NotificationCompat.Builder(
                this,
                channelId
            )
                .setOngoing(true)
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .build()
        )
    }

    override fun onDestroy() {
        httpServer?.stop()

        super.onDestroy()
    }

    companion object {
        const val PORT_EXTRA = "PORT"
        const val IP4_EXTRA = "IP4"
    }
}