package com.e.httpaccess.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class CallStatusReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER) == null) {
            return
        }
        val number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)
        val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)
        _callState.postValue(state)
        _callNumber.postValue(number)
    }

    companion object {
        private val _callState = MutableLiveData<String>()
        val callState: LiveData<String> = _callState
        private val _callNumber = MutableLiveData<String>()
        val callNumber: LiveData<String> = _callNumber
    }
}