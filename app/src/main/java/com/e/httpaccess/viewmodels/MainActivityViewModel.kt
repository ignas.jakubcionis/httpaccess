package com.e.httpaccess.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.e.httpaccess.database.CallLogRepository
import com.e.httpaccess.model.CallLogEntry
import com.e.httpaccess.network.NetworkInfo
import java.net.InetAddress

class MainActivityViewModel(callLogRepository: CallLogRepository) : ViewModel() {

    private val _serverFullAddress = MutableLiveData<String>()
    val serverFullAddress: LiveData<String> = _serverFullAddress
    private val _serverPort = MutableLiveData<Int>()
    val serverPort: LiveData<Int> = _serverPort
    private val _serverIp4 = MutableLiveData<String>()
    val serverIp4: LiveData<String> = _serverIp4
    private val _callLogs = MutableLiveData<List<Pair<String, String>>>()
    val callLogs: LiveData<List<Pair<String, String>>> = _callLogs

    init {
        val inetAddress = NetworkInfo.findIp4()[0]
        val ip4 = inetAddress.hostAddress
        val port = NetworkInfo.findForOpenPort(inetAddress as InetAddress, 8080)

        _serverPort.value = port
        _serverIp4.value = ip4
        _serverFullAddress.value = "$ip4:$port"

        callLogEntriesToMapList(callLogRepository.getAllCallLogs()).let {
            _callLogs.value = it
        }
    }

    private fun callLogEntriesToMapList(callLogList: List<CallLogEntry>): List<Pair<String, String>> {
        val callLogMapList = ArrayList<Pair<String, String>>()
        for (callLogEntry in callLogList) {
            callLogMapList.add(
                Pair(
                    callLogEntry.name,
                    secondsToTimeString(callLogEntry.duration)
                )
            )
        }
        return callLogMapList
    }

    private fun secondsToTimeString(callTimeSeconds: String): String {
        val callTime = Integer.parseInt(callTimeSeconds)
        val hours = callTime / 3600
        val minutes = (callTime % 3600) / 60
        val seconds = callTime % 60
        return "$hours h : $minutes m : $seconds s"
    }
}