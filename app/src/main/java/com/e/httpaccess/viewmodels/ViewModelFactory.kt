package com.e.httpaccess.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.e.httpaccess.database.CallLogRepository

@Suppress("UNCHECKED_CAST")
class ViewModelFactory constructor(private val repository: CallLogRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>)
            : T = MainActivityViewModel(repository) as T
}